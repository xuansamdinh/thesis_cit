#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "xuansamdinh"
__copyright__ = "Copyright (c) 2012 xuansamdinh"
__credits__ = ["xuansamdinh"]
__email__ = "xuansamdinh.n2i@gmail.com"

__version__ = "0.0.1"
__liciense__ = "GPL"
__maintainer__ = ["xuansamdinh"]
__status__ = "Development"

import cStringIO
import pycurl
import sys


from xml.etree import ElementTree as ET

KEY = "c38ab9585c6cc0a837140c2fdb435a2c"
API_URL = "http://api.imgur.com/2/"
FORMAT = "xml"

class Imgur(object):
    def __init__(self, img):
        self.upload_url = API_URL + 'upload.' + FORMAT
        self.stats_url = API_URL + 'stats.' + FORMAT
        self.album_url = API_URL + 'album/'
        self.upload_url = API_URL + 'upload/'
        self.key = KEY
        self.img = img
        self.data = ""
        self.imginfo = {}
        self.links = {}
        #self.img = sys.argv[1]

    def upload_anony(self):
        up = pycurl.Curl()
        buf = cStringIO.StringIO()
        key = ("key", self.key)
        values = [
                key,
                ("image", (up.FORM_FILE, self.img))]
        up.setopt(up.URL, self.upload_url)
        up.setopt(up.HTTPPOST, values)
        up.setopt(up.VERBOSE, True)
        up.setopt(up.WRITEFUNCTION, buf.write)
        up.perform()
        up.close()
        return buf.getvalue()

    def getinfo(self, dataxml):
        data = ET.fromstring(dataxml)
        imginfo = {}
        links = {}

        for value in data.findall("image/"):
            imginfo[value.tag] = value.text
        for value in data.findall("links/"):
            links[value.tag] = value.text

        return imginfo, links

    def print_info(self, imginfo, links):
        for value in imginfo.keys():
            print "%15s: %s" % (value, imginfo[value])
        for value in links.keys():
            print "%15s: %s" % (value, links[value])

def main():
    if len(sys.argv[:]) == 1:
        print "Please specify an image to upload"
        sys.exit(2)
    else:
        new = Imgur(sys.argv[1])
        new.data = new.upload_anony()
        new.imginfo, new.links = new.getinfo(new.data)
        new.print_info(new.imginfo, new.links)


if __name__ == "__main__":
    main()
    sys.exit()
