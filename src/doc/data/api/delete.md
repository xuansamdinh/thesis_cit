#/delete/:DELETE\_HASH
******
Deletes an image

**URL:**[http://api.imgur.com/2/delete/:DELETE\_HASH][1]  
##Request method:
**GET** : Delete image  
**DELETE**: Delete image  
##Response:
[**delete.xml**][2]

[1]: http://api.imgur.com/2/delete/:DELETE_HASH
[2]: ../response/delete.xml
>  vim: set ff=unix ft=markdown fenc=utf-8 ts=4 sw=4 tw=79 :
