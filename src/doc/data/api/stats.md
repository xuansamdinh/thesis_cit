#/stats
**Description**
Display site statistics, such as bandwidth usage, images uploaded, image views,
and average image size.

**URL:** [http://api.imgur.com/2/stats][1]  

##Request method:
**GET** Display site's statistics

##Parameter:
| Parameter | Required | Request Method | Description |
| *view*    | optional | **GET**        | today, week, month |
##Response:
<stats>
    <most_popular_images>
        <image_hash>gn2gN</image_hash>
        <image_hash>cXQqZ</image_hash>
        <image_hash>JiPqw</image_hash>
        <image_hash>WdRim</image_hash>
        <image_hash>zPxWo</image_hash>
    </most_popular_images>
    <images_uploaded>1627648</images_uploaded>
    <images_veiwed>1080303365</images_veiwed>
    <bandwidth_used>4.58 TB</bandwidth_used>
    <average_image_size>"40.74KB"</average_image_size>
</stats>

[1]: http://api.imgur.com/2/stats
>  vim: set ff=unix ft=markdown fenc=utf-8 ts=4 sw=4 tw=79 :
