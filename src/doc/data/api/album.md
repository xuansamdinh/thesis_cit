#/album/:ID
Returns album information and lists all images that belong to the album.  

**URL:** [http://api.imgur.com/2/album/:ID][1]  

##Method
| **Request method** | **Description** |  
| **GET**                | Album & image info |  

##Response:

    <album>
        <title>Album Title</title>
        <description>Album Description</description>
        <cover>ix7Yt</cover>
        <layout>grid</layout>
        <images>
            <item>
                <image>
                    <title/>
                    <caption/>
                    <hash>ix7Yt</hash>
                    <datetime>2012-03-22 21:47:52</datetime>
                    <type>image/png</type>
                    <animated>false</animated>
                    <width>1000</width>
                    <height>520</height>
                    <size>15827</size>
                    <views>0</views>
                    <bandwidth>0</bandwidth>
                </image>
                <links>
                <original>http://i.joshbox.imgur.com/ix7Yt.png</original>
                <imgur_page>http://joshbox.imgur.com/ix7Yt</imgur_page>
                <small_square>http://i.joshbox.imgur.com/ix7Yts.jpg</small_square>
                <large_thumbnail>http://i.joshbox.imgur.com/ix7Ytl.jpg</large_thumbnail>
                </links>
            </item>
        </images>
    </album>

[1]: http://api.imgur.com/2/album/:ID
