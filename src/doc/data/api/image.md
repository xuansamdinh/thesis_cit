#/image/:HASH
******
Returns all the information about a certain image

**URL:** [http://api.imgur.com/2/image/:HASH][1]

#Request method:
**GET** : Returns all the information about the image

#Response:
[**stats.xml**][2]

[1]: http://api.imgur.com/2/image/:HASH 
[2]: ../response/stats.xml
>  vim: set ff=unix ft=markdown fenc=utf-8 ts=4 sw=4 tw=79 :
